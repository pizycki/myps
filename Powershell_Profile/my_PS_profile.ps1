# Functions (a.k.a. helpers)

#----------------------------------------------------------------------------
# Adds all files to commit and commits with given msg.
# Author: pizycki
function Git-AddAll-AndCommit($message)
{ 
	git add -A
	git commit -m $message
}


#----------------------------------------------------------------------------
# Short for 'git status'
function Git-Status { git status }


#----------------------------------------------------------------------------
# Removes bin and debug directiories within all solution.
# Author: pizycki
function Clear-Solution
{
	Get-ChildItem -Recurse `
		| Where-Object { ($_.Name -eq "bin") -or ($_.Name -eq "obj") } `
		| ?{ $_.PSIsContainer } `
		| Remove-Item -Recurse -Force 
} #-or ($_.Name -eq "packages") # VS blocks access to .dlls in packages dir, so they cannot be deleted without closing sln.


#----------------------------------------------------------------------------
# Creates gitignore file.
# Author: pizycki
$GitIgnoreTemplateSources =
@{
	VisualStudio="https://raw.githubusercontent.com/pawelizycki/gitignore/master/VisualStudio.gitignore";
}

function Create-GitIgnoreFile([string] $template)
{
	# Download gitignore file content and save it to file.
	$url = $GitIgnoreTemplateSources[$template]
	(new-object net.webclient).DownloadString($url) `
		| Out-File -Encoding utf8 -FilePath ".gitignore"
}

function Create-GitIgnoreFile-VisualStudio
{
	Create-GitIgnoreFile("VisualStudio")
}

#----------------------------------------------------------------------------
# Adds new location to PATH variable.
# Author: pizycki
function Append-PathVariable($newLoc, $scope = "Machine")
{   
	[string] $path = [System.Environment]::GetEnvironmentVariable("PATH", $scope)
	if($path.Contains($newLoc))
	{
		write "This location already exists in PATH variable. Skipping..."
	}
	else
	{
		[string] $newPath = $path+';'+$newLoc
		write "New PATH: " $newPath
		[System.Environment]::SetEnvironmentVariable("PATH", $newPath, $scope)
	}
}


#----------------------------------------------------------------------------
# Changes directory to actually logged users desktop.
# Author: pizycki
function JumpTo-Desktop(){
    $loc = [Environment]::GetFolderPath("Desktop")
    cd $loc
}

#----------------------------------------------------------------------------
function gitignore {
  param(
    [Parameter(Mandatory=$true)]
    [string[]]$list
  )
  $params = $list -join ","
  Invoke-WebRequest -Uri "https://www.gitignore.io/api/$params" | select -ExpandProperty content | Out-File -FilePath $(Join-Path -path $pwd -ChildPath ".gitignore") -Encoding ascii
}

#----------------------------------------------------------------------------
function Remove-Directory($path)
{
	Remove-Item $path -Recurse -Force	
}


# Alliases
Set-Alias sublime "C:\Program Files\Sublime Text 3\subl.exe"
Set-Alias gs Git-Status
Set-Alias gitignore-vs Create-GitIgnoreFile-VisualStudio
Set-Alias desktop JumpTo-Desktop
Set-Alias rm-dir Remove-Directory

Write-Host "Custom powershell profile loaded!"