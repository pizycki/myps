[CmdletBinding()]
Param(
    [parameter()]
    [string]$myProfileScript = "my_PS_profile.ps1"                                      # My profile to be copied.
)

[string] $psProfileDir = $env:USERPROFILE+"\Documents\WindowsPowerShell\"               # Powershell profile directory where profile should be placed.
[string] $profileName = "profile.ps1"                                                   # Original profile file name.
[string] $profilePath = [string]::Format("{0}\{1}", $psProfileDir, $profileName)        # Full path to original profile.

# ONLY FOR DEBUG
#----------------
#Remove-Item $psProfileDir -Recurse -Force

#----------------

Try
{
    # Make sure that PowerShell profile directory exists.
    if (!(Test-Path($psProfileDir)))
    {
         New-Item -Path $psProfileDir -ItemType directory | Out-Null
    }

    if (Test-Path($profilePath))
    {        
        # If profile file present, make a backup.
        $backupName = [string]::Format("{0}.back_{1}", $profileName, [DateTime]::Now.ToString("yyyyMMddHHmmss"))
        Rename-Item $profilePath -newName $backupName   # Rename backup
    }

    # Copy Powershell Profile script file to Powershell profile directory.
    Copy-Item -Path $myProfileScript -Destination $psProfileDir
    Rename-Item $psProfileDir"\"$myProfileScript -newName $profileName

    Write "Powershell profile saved in: [ $profilePath ]"
}
Catch
{
    Write-Error "Script failed!"
}