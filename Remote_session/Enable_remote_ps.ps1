Enable-PSRemoting -SkipNetworkProfileCheck -Force

# If not in the same domain then add trusted hosts to both computers.
# Set-Item wsman:\localhost\client\trustedhosts * (or specific computer names).
# and follow it with restart:
# Restart-Service WinRM

# For testing use:
#Test-WsMan COMPUTER_NAME

# For testing commands use:
# Invoke-Command -ComputerName COMPUTER_NAME -ScriptBlock { whoami.exe }