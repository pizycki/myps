[CmdletBinding()]
Param(
    [parameter()]
    [string]$email = "pawelizycki@gmail.com",

    [parameter()]
    [string]$username = "Paweł Iżycki"
)

# For more info visit: https://git-scm.com/book/en/v1/Getting-Started-First-Time-Git-Setup

# Set name and email
git config --global user.email $email -Verbose
git config --global user.name $username

# Set default editor (i.e. for commiting)
git config --global core.editor sublime

# Set default diff tool
git config --global merge.tool kdiff3

write "Git Config changes { user.email: $email, user.name: $username }"